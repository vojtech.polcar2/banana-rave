# BANANA RAVE
Semestrální práce pro předmět MI-APH.

Postup pro spuštění hry:

``git clone git@gitlab.fit.cvut.cz:polcavoj/mi-aph.git``

``npm install``

``npm run start``

V prohlížeči přejděte na adresu: ``http://localhost:1234``

## O hře
Jedná se o jednoduchou hru ve které máte za úkol jako Minion z filmů od Pixaru získat co největší skóre za sbírání banánů.
V tom se Vám snaží zabránit opice, kterým banány kradete. Hra je postavena na základě ECSA architektury a ke komunikaci
využívá Messaging a identifikaci pomocí Names a States.

Hra dosahuje několika stavů při kterých přibývají druhy padajících předmětů a při překročení 100 bodů skóre se objevují i
zákeřné opice, které vám mohou způsobit stun.

##### Ovládání
 * Pohyb doleva - LEFT, A
 * Pohyb doprava - RIGHT, D
 * Pauza/Pokračovat/Start - SPACE
 * Restart hry - R

## Architektura

![alt text](./architecture.png "Architektura")
