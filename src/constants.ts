export enum Assets {
  BANANA = 'banana',
  BANANA_CLUMP = 'banana_clump',
  BANANA_PEEL = 'banana_peel',
  DATA = 'data_json',
  MINION = 'minion',
  MINION_HITTED = 'minion_hitted',
  MONKEY_POOP = 'monkey_poop',
  SND_BANANA = 'snd_banana',
  SND_GAMEOVER = 'snd_gameover',
  SND_INTRO = 'snd_intro',
  SND_MISSED = 'snd_missed',
  SND_MONKEY = 'snd_monkey',
  SND_PEEL = 'snd_peel',
  SND_SPLASH = 'snd_splash',
}

export enum Attributes {
  COLLISION_MANAGER = 'COLLISION_MANAGER',
  DYNAMICS = 'DYNAMICS',
  MODEL = 'MODEL',
  FACTORY = 'FACTORY',
}

export enum Messages {
  BANANA_MISSED = 'banana_missed',
  BANANA_CATCHED = 'banana_catched',
  BANANA_CLUMP_CATCHED = 'banana_clump_catched',
  BANANA_PEEL_CATCHED = 'banana_peel_catched',
  GAME_OVER = 'game_over',
  GAME_START = 'game_start',
  GAME_RESTART = 'game_restart',
  GAME_RUNNING = 'game_running',
  GAME_PAUSED = 'game_paused',
  ITEM_CREATED = 'item_created',
  MONKEY = 'monkey',
  POOP_HIT = 'poop_hit',
}

export enum Names {
  BANANA = 'banana',
  BANANA_CLUMP = 'banana_clump',
  BANANA_PEEL = 'banana_peel',
  MONKEY_POOP = 'monkey_poop',
  CONTINUE = 'continue',
  GAMEOVER = 'gameover',
  GROUND = 'ground',
  GRASS = 'grass',
  MENU = 'menu',
  MINION = 'minion',
  MINION_HITTED = 'minion_hitted',
  MISSED_BANANAS = 'missed_bananas',
  PALM_PART = 'palm_part',
  PALM_LEAF = 'palm_leaf',
  PLAYER = 'player',
  SKY = 'sky',
  SCORE = 'score',
}

export const MONKEY_IMGS_FOR_ANIM_ON = ['../assets/monkey/monkey1.png','../assets/monkey/monkey2.png','../assets/monkey/monkey3.png','../assets/monkey/monkey4.png','../assets/monkey/monkey5.png','../assets/monkey/monkey6.png','../assets/monkey/monkey7.png','../assets/monkey/monkey8.png','../assets/monkey/monkey9.png','../assets/monkey/monkey10.png'];
export const MONKEY_IMGS_FOR_ANIM_OFF = ['../assets/monkey/monkey10.png','../assets/monkey/monkey9.png','../assets/monkey/monkey8.png','../assets/monkey/monkey7.png','../assets/monkey/monkey6.png','../assets/monkey/monkey5.png','../assets/monkey/monkey4.png','../assets/monkey/monkey3.png','../assets/monkey/monkey2.png','../assets/monkey/monkey1.png'];

export enum States {
  FALLING = 0,
  ON_THE_GROUND = 1,
  BANANAS = 2,
  BANANA_CLUMPS = 3,
  BANANA_PEELS = 4,
  ANGRY_MONKEY = 5,
}

export const SCREEN_WIDTH = 600;
export const SCREEN_HEIGHT = 600;
export const BLOCK_SIZE = 50;
