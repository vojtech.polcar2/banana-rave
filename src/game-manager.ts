import * as ECSA from '../libs/pixi-component';
import {BananaRaveBaseComponent} from './banana-rave-base-component';
import {Messages, Names} from './constants';

export class GameManager extends BananaRaveBaseComponent {
  onInit() {
    super.onInit();
    this.subscribe(Messages.BANANA_MISSED, Messages.BANANA_CATCHED, Messages.BANANA_CLUMP_CATCHED,
      Messages.BANANA_PEEL_CATCHED, Messages.POOP_HIT);
  }

  onMessage(msg: ECSA.Message) {
    if (this.model.isGameOver) {
      return;
    }

    if (msg.action === Messages.BANANA_MISSED) {
      this.model.missedBananas++;
      if (this.model.missedBananas >= this.model.maxMissedBananas) {
        this.gameOver();
      }
    }

    if (msg.action === Messages.BANANA_CATCHED) {
      this.model.score += this.model.bananaReward;
    }

    if (msg.action === Messages.BANANA_CLUMP_CATCHED) {
      this.model.score += this.model.bananaClumpReward;
    }

    if(msg.action === Messages.BANANA_PEEL_CATCHED) {
      this.model.score += this.model.bananaPeelPenalty;
      if (this.model.score < 0) {
        this.gameOver();
      }
    }

    if(msg.action === Messages.POOP_HIT) {
      this.factory.createHittedMinion(this.owner, new ECSA.Builder(this.owner.scene));
    }
  }

  protected gameOver() {
    this.scene.findObjectByName(Names.GAMEOVER).pixiObj.visible = true;
    this.scene.findObjectByName(Names.CONTINUE).pixiObj.visible = true;
    this.model.isGameOver = true;
    this.sendMessage(Messages.GAME_OVER);
  }
}
