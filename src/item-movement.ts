import * as ECSA from '../libs/pixi-component';
import DynamicsComponent from './utils/dynamics-component';
import {GameModel} from './game-model';
import {Attributes, Messages, Names, States} from './constants';
import {GameObject, Vector} from '../libs/pixi-component';

export class ItemMovement extends DynamicsComponent {
  model: GameModel;
  grass: GameObject;
  lastState: number;
  gameIsRunning: boolean;
  inDeleteMode: boolean;

  constructor() {
    super(Attributes.DYNAMICS);
    this.gameIsRunning = true;
    this.lastState = States.FALLING;
    this.inDeleteMode = false;
  }

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_START, Messages.GAME_RUNNING, Messages.GAME_PAUSED, Messages.GAME_OVER);
    this.model = this.scene.getGlobalAttribute(Attributes.MODEL);
    this.grass = this.scene.findObjectByName(Names.GRASS);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START || msg.action === Messages.GAME_RUNNING) {
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_PAUSED || msg.action === Messages.GAME_OVER) {
      this.gameIsRunning = false;
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.gameIsRunning && this.lastState === States.FALLING) {
      this.checkGrassCollision();
      this.checkMinionCollision();
      super.onUpdate(delta, absolute);
    }

    if (this.lastState === States.ON_THE_GROUND && !this.inDeleteMode) {
      this.scene.invokeWithDelay(10000, () => this.owner.pixiObj.destroy());
      this.inDeleteMode = true;
    }
  }

  checkGrassCollision() {
    let grassBB = this.grass.pixiObj.getBounds();
    let itemBB = this.owner.pixiObj.getBounds();

    if (itemBB.bottom >= grassBB.top) {
      this.dynamics.velocity = new Vector(0, 0);
      this.dynamics.aceleration = new Vector(0, 0);
      if (this.owner.pixiObj.name === Names.BANANA || this.owner.pixiObj.name === Names.BANANA_CLUMP) {
        this.sendMessage(Messages.BANANA_MISSED);
      }
      this.model.itemsOnScene--;
      this.lastState = States.ON_THE_GROUND;
    }
  }

  checkMinionCollision() {
    let minionBB = this.scene.findObjectByName(Names.MINION).pixiObj.getBounds();
    let itemBB = this.owner.pixiObj.getBounds();

    if ( itemBB.bottom > minionBB.top && (itemBB.left < minionBB.right && itemBB.right > minionBB.left)) {
      if (this.owner.pixiObj.name === Names.BANANA) {
        this.sendMessage(Messages.BANANA_CATCHED);
      } else if (this.owner.pixiObj.name === Names.BANANA_CLUMP) {
        this.sendMessage(Messages.BANANA_CLUMP_CATCHED);
      } else if (this.owner.pixiObj.name === Names.BANANA_PEEL) {
        this.sendMessage(Messages.BANANA_PEEL_CATCHED);
      }
      this.model.itemsOnScene--;
      this.lastState = States.ON_THE_GROUND;
      this.inDeleteMode = true;
      this.scene.invokeWithDelay(100, () => this.owner.pixiObj.destroy());
    }
  }
}
