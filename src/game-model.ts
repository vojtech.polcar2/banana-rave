export class GameModel {
  // ========================= dynamic data
  score: number = 0;
  missedBananas: number = 0;
  isGameOver = false;
  itemsOnScene = 0;
  monkeyOnScene = false;
  // =========================


  // ========================= static data
  maxMissedBananas: number;
  maxItemsOnScene: number;
  itemSpawnFrequency: number;
  bananaClumpProbability: number;
  bananaPeelProbability: number;
  bananaReward: number;
  bananaClumpReward: number;
  bananaPeelPenalty: number;
  monkeyPoopStunDelay: number;
  gravity: number;

  // positions for copter spawning (in relative units)
  itemSpawnMinY: number;
  itemSpawnMaxY: number;
  itemSpawnMinX: number;
  itemSpawnMaxX: number;

  itemMinVelocity: number;
  itemMaxVelocity: number;
  /**
   * Loads model from JSON structure
   */
  loadModel(data: any) {
    this.maxMissedBananas = data.max_missed_bananas;
    this.maxItemsOnScene = data.max_items_on_scene;
    this.itemSpawnFrequency = data.item_spawn_frequency;
    this.bananaClumpProbability = data.banana_clump_probability;
    this.bananaPeelProbability = data.banana_peel_probability;
    this.bananaReward = data.banana_reward;
    this.bananaClumpReward = data.banana_clump_reward;
    this.bananaPeelPenalty = data.banana_peel_penalty;
    this.monkeyPoopStunDelay = data.monkey_poop_stun_delay;
    this.gravity = data.gravity;
    this.itemSpawnMinY = data.item_spawn_min_y;
    this.itemSpawnMaxY = data.item_spawn_max_y;
    this.itemSpawnMinX = data.item_spawn_min_x;
    this.itemSpawnMaxX = data.item_spawn_max_x;
    this.itemMinVelocity = data.item_min_velocity;
    this.itemMaxVelocity = data.item_max_velocity;
  }

  /**
   * Resets dynamic data
   */
  reset() {
    this.score = 0;
    this.missedBananas = 0;
    this.isGameOver = false;
    this.itemsOnScene = 0;
    this.monkeyOnScene = false;
  }
}
