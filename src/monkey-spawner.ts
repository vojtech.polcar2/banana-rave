import * as ECSA from '../libs/pixi-component';
import {BananaRaveBaseComponent} from './banana-rave-base-component';
import {checkTime} from './utils/functions';
import {Messages, States} from './constants';

export class MonkeySpawner extends BananaRaveBaseComponent {
  lastSpawnTime = 0;
  spawnFrequency = 0.1;
  gameStarted: boolean;
  gameIsRunning: boolean;
  lastState: number;

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_START, Messages.GAME_RUNNING, Messages.GAME_PAUSED, Messages.GAME_OVER);
    this.gameStarted = false;
    this.gameIsRunning = false;
    this.lastState = States.BANANAS;
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START) {
      this.gameStarted = true;
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_PAUSED) {
      this.gameIsRunning = false;
    } else if(msg.action === Messages.GAME_RUNNING) {
      this.gameIsRunning = true;
    } else if (Messages.GAME_OVER) {
      this.gameIsRunning = false;
      this.gameStarted = false;
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.model.score > 10 && this.model.score <= 50 && this.lastState !== States.BANANA_CLUMPS) {
      this.lastState = States.BANANA_CLUMPS;
    } else if (this.model.score > 50 && this.model.score <= 100 && this.lastState !== States.BANANA_PEELS) {
      this.lastState = States.BANANA_PEELS;
    } else if (this.model.score > 100 && this.lastState !== States.ANGRY_MONKEY) {
      this.lastState = States.ANGRY_MONKEY;
    }

    if (this.gameIsRunning && this.gameStarted && !this.model.monkeyOnScene && this.lastState === States.ANGRY_MONKEY) {
      if (checkTime(this.lastSpawnTime, absolute, this.spawnFrequency)) {
        this.lastSpawnTime = absolute;
        this.factory.createMonkey(this.owner, new ECSA.Builder(this.owner.scene));
        this.model.monkeyOnScene = true;
      }
    }
  }

}
