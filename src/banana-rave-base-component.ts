import * as ECSA from '../libs/pixi-component';
import {GameModel} from './game-model';
import GameFactory from './game-factory';
import {Attributes} from './constants';

export class BananaRaveBaseComponent extends ECSA.Component {
  model: GameModel;
  factory: GameFactory;

  onInit() {
    this.model = this.scene.getGlobalAttribute<GameModel>(Attributes.MODEL);
    this.factory = this.scene.getGlobalAttribute<GameFactory>(Attributes.FACTORY);
  }
}
