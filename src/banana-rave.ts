import * as ECSA from '../libs/pixi-component';
import GameFactory from './game-factory';
import {Assets, SCREEN_HEIGHT, SCREEN_WIDTH} from './constants';

class BananaRave {
  engine: ECSA.GameLoop;

  constructor() {
    this.engine = new ECSA.GameLoop();
    let canvas = (document.getElementById('gameCanvas') as HTMLCanvasElement);

    // init the game loop
    this.engine.init(canvas, SCREEN_WIDTH, SCREEN_HEIGHT, 1, // width, height, resolution
      {
       flagsSearchEnabled: false, // searching by flags feature
       statesSearchEnabled: true, // searching by states feature
       tagsSearchEnabled: false, // searching by tags feature
       namesSearchEnabled: true, // searching by names feature
       notifyAttributeChanges: false, // will send message if attributes change
       notifyStateChanges: false, // will send message if states change
       notifyFlagChanges: false, // will send message if flags change
       notifyTagChanges: false, // will send message if tags change
       debugEnabled: false // debugging window
     }, true); // resize to screen

    this.engine.app.loader
      .reset()
      .add(Assets.DATA, './assets/config.json')
      .add(Assets.BANANA, './assets/banana.png')
      .add('monkey1', './assets/monkey1.png')
      .add('monkey2', './assets/monkey2.png')
      .add('monkey3', './assets/monkey3.png')
      .add('monkey4', './assets/monkey4.png')
      .add('monkey5', './assets/monkey5.png')
      .add('monkey6', './assets/monkey6.png')
      .add('monkey7', './assets/monkey7.png')
      .add('monkey8', './assets/monkey8.png')
      .add('monkey9', './assets/monkey9.png')
      .add('monkey10', './assets/monkey10.png')
      .add(Assets.BANANA_CLUMP, './assets/banana_clump.png')
      .add(Assets.BANANA_PEEL, './assets/banana_peel.png')
      .add(Assets.MINION, './assets/minion.png')
      .add(Assets.MINION_HITTED, './assets/minion-hitted.png')
      .add(Assets.MONKEY_POOP, './assets/poop.png')
      .add(Assets.SND_BANANA, './assets/banana.mp3')
      .add(Assets.SND_GAMEOVER, './assets/gameover.mp3')
      .add(Assets.SND_INTRO, './assets/intro.mp3')
      .add(Assets.SND_MISSED, './assets/uhoh.mp3')
      .add(Assets.SND_MONKEY, './assets/monkey.mp3')
      .add(Assets.SND_PEEL, './assets/peel_catch.mp3')
      .add(Assets.SND_SPLASH, './assets/splash.mp3')
      .load(() => this.onAssetsLoaded());
  }

  onAssetsLoaded() {
    let factory = new GameFactory();
    factory.resetGame(this.engine.scene);
  }
}

export default new BananaRave();
