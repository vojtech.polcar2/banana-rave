import * as ECSA from '../libs/pixi-component';
import {BananaRaveBaseComponent} from './banana-rave-base-component';
import {Messages, MONKEY_IMGS_FOR_ANIM_OFF, MONKEY_IMGS_FOR_ANIM_ON} from './constants';
import Dynamics from './utils/dynamics';

export class MonkeyComponent extends BananaRaveBaseComponent {
  monkeyInDeleteMode: boolean;
  monkeyFadingIn: PIXI.AnimatedSprite;
  monkeyFadingOut: PIXI.AnimatedSprite;
  rootObject: ECSA.Container;
  builder: ECSA.Builder;
  gameStarted: boolean;
  gameIsRunning: boolean;
  monkeyFadingInDelete: boolean;
  monkeyFadingOutDelete: boolean;
  posX: number;
  posY: number;

  constructor(rootObject: ECSA.Container, builder: ECSA.Builder) {
    super();
    this.rootObject = rootObject;
    this.builder = builder;
    this.monkeyInDeleteMode = false;
    this.monkeyFadingIn = null;
    this.monkeyFadingOut = null;
    this.gameStarted = false;
    this.gameIsRunning = false;
    this.monkeyFadingInDelete = false;
    this.monkeyFadingOutDelete = false;
    this.posX = 0;
    this.posY = 0;
    this.createMonkeyAnimation();
  }

  createMonkeyAnimation() {
    let selectOnePosition = Math.random();
    if (selectOnePosition > 0.5) {
      this.posY = 100;
      this.posX = 150;
    } else {
      this.posY = 120;
      this.posX = 356;
    }

    let texturesFadeIn = [];
    let texturesFadeOut = [];

    for( let i = 0; i < 10; i++) {
      let textureFadeIn = PIXI.Texture.from(MONKEY_IMGS_FOR_ANIM_ON[i]);
      let textureFadeOut = PIXI.Texture.from(MONKEY_IMGS_FOR_ANIM_OFF[i]);
      texturesFadeIn.push(textureFadeIn);
      texturesFadeOut.push(textureFadeOut);
    }

    this.monkeyFadingIn = new PIXI.AnimatedSprite(texturesFadeIn);
    this.monkeyFadingOut = new PIXI.AnimatedSprite(texturesFadeOut);
    this.monkeyFadingIn.animationSpeed = 0.1;
    this.monkeyFadingOut.animationSpeed = 0.1;

    this.monkeyFadingIn.loop = false;
    this.monkeyFadingOut.loop = false;

    this.monkeyFadingIn.position.set(this.posX,this.posY);
    this.monkeyFadingOut.position.set(this.posX,this.posY);

    this.rootObject.addChild(this.monkeyFadingIn);
    this.rootObject.addChild(this.monkeyFadingOut).visible = false;
    this.monkeyFadingIn.play();
  }

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_START, Messages.GAME_RUNNING, Messages.GAME_PAUSED, Messages.GAME_OVER);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START) {
      this.gameStarted = true;
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_PAUSED) {
      this.gameIsRunning = false;
      if (!this.monkeyFadingInDelete) {
        this.monkeyFadingIn.stop();
      }
      if (!this.monkeyFadingOutDelete) {
        this.monkeyFadingOut.stop();
      }
    } else if(msg.action === Messages.GAME_RUNNING) {
      this.gameIsRunning = true;
      if (!this.monkeyFadingInDelete) {
        this.monkeyFadingIn.play();
      }
      if (!this.monkeyFadingOutDelete) {
        this.monkeyFadingOut.play();
      }
    } else if (Messages.GAME_OVER) {
      this.gameIsRunning = false;
      this.gameStarted = false;
      if (!this.monkeyFadingInDelete) {
        this.monkeyFadingIn.stop();
      }
      if (!this.monkeyFadingOutDelete) {
        this.monkeyFadingOut.stop();
      }
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.monkeyFadingIn && !this.monkeyFadingInDelete) {
      if(this.monkeyFadingIn.currentFrame === this.monkeyFadingIn.totalFrames - 1) {
        this.monkeyFadingInDelete = true;
        this.sendMessage(Messages.MONKEY);
        this.owner.scene.invokeWithDelay(2000, () => this.monkeyFadingIn.destroy());
        this.owner.scene.invokeWithDelay(2000, () => {
          this.monkeyFadingOut.visible = true;
          this.monkeyFadingOut.play();
          let dynamics = new Dynamics();
          dynamics.velocity = new ECSA.Vector(0, 300);
          this.factory.createMonkeyPoop(this.owner, new ECSA.Builder(this.owner.scene), this.posX, this.posY, dynamics);
        });
      }
    }

    if (this.monkeyFadingOut && !this.monkeyFadingOutDelete) {
      if(this.monkeyFadingOut.currentFrame === this.monkeyFadingOut.totalFrames - 1) {
        this.monkeyFadingOutDelete = true;
        this.model.monkeyOnScene = false;
        this.monkeyFadingOut.destroy();
      }
    }
  }
}
