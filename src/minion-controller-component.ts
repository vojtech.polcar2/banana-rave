import * as ECSA from '../libs/pixi-component';
import {BananaRaveBaseComponent} from './banana-rave-base-component';
import {GameModel} from './game-model';
import GameFactory from './game-factory';
import { Messages, SCREEN_WIDTH} from './constants';
import {checkTime} from './utils/functions';

export class MinionControllerComponent extends BananaRaveBaseComponent {
  model: GameModel;
  factory: GameFactory;
  cmp: ECSA.KeyInputComponent;
  gameStarted: boolean;
  gameIsRunning: boolean;
  gameOver: boolean;
  stunned: boolean;
  stunTime: number;
  protected lastUpdate = 0;

  constructor() {
    super();
    this.gameStarted = false;
    this.gameIsRunning = false;
    this.gameOver = false;
    this.stunned = false;
    this.stunTime = -1;
  }

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_PAUSED, Messages.GAME_START, Messages.GAME_RUNNING, Messages.GAME_RESTART, Messages.GAME_OVER, Messages.POOP_HIT);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START) {
      this.gameStarted = true;
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_PAUSED) {
      this.gameIsRunning = false;
    } else if(msg.action === Messages.GAME_RUNNING) {
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_RESTART) {
      this.gameStarted = true;
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_OVER) {
      this.gameOver = true;
      this.gameIsRunning = false;
      this.gameStarted = false;
    } else if (msg.action === Messages.POOP_HIT) {
      this.stunned = true;
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.cmp === undefined) {
      this.cmp = this.scene.stage.findComponentByName<ECSA.KeyInputComponent>(ECSA.KeyInputComponent.name);
    }
    if (this.stunned === true) {
      if (this.stunTime === -1) {
        this.stunTime = absolute;
      } else {
        if(checkTime(this.stunTime, absolute, 0.5)) {
          this.stunTime = -1;
          this.stunned = false;
        }
      }
    } else {
      if (this.gameStarted && this.gameIsRunning && !this.gameOver && (this.cmp.isKeyPressed(ECSA.Keys.KEY_A) || this.cmp.isKeyPressed(ECSA.Keys.KEY_LEFT))) {
        this.move(true, delta);
      }
      if (this.gameStarted && this.gameIsRunning && !this.gameOver && (this.cmp.isKeyPressed(ECSA.Keys.KEY_D) || this.cmp.isKeyPressed(ECSA.Keys.KEY_RIGHT))) {
        this.move(false, delta);
      }
    }
  }

  move(left: boolean, delta: number) {
    if (left) {
      if (this.owner.pixiObj.position.x > (SCREEN_WIDTH/100) * 10) {
        this.owner.pixiObj.position.x -= 0.25 * delta;
      }
    } else {
      if (this.owner.pixiObj.position.x < (SCREEN_WIDTH/100) * 80) {
        this.owner.pixiObj.position.x += 0.25 * delta;
      }
    }
  }
}
