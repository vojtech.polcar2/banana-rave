import * as ECSA from '../libs/pixi-component';
import DynamicsComponent from './utils/dynamics-component';
import {GameModel} from './game-model';
import {Attributes, Messages, Names, States} from './constants';
import {GameObject, Vector} from '../libs/pixi-component';

export class PoopMovement extends DynamicsComponent {
  model: GameModel;
  grass: GameObject;
  lastState: number;
  gameIsRunning: boolean;
  poopIsDeleted: boolean;

  constructor() {
    super(Attributes.DYNAMICS);
    this.gameIsRunning = true;
    this.lastState = States.FALLING;
    this.poopIsDeleted = false;
  }

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_START, Messages.GAME_RUNNING, Messages.GAME_PAUSED, Messages.GAME_OVER);
    this.model = this.scene.getGlobalAttribute(Attributes.MODEL);
    this.grass = this.scene.findObjectByName(Names.GRASS);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START || msg.action === Messages.GAME_RUNNING) {
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_PAUSED || msg.action === Messages.GAME_OVER) {
      this.gameIsRunning = false;
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.gameIsRunning && !this.poopIsDeleted && this.lastState !== States.ON_THE_GROUND) {
      this.checkMinionCollision();
      this.checkGrassCollision();
      super.onUpdate(delta, absolute);
    }
  }

  checkGrassCollision() {
    let grassBB = this.grass.pixiObj.getBounds();
    let itemBB = this.owner.pixiObj.getBounds();

    if (itemBB.bottom >= grassBB.top) {
      this.dynamics.velocity = new Vector(0, 0);
      this.dynamics.aceleration = new Vector(0, 0);
      this.lastState = States.ON_THE_GROUND;
      this.poopIsDeleted = true;
      this.scene.invokeWithDelay(100, () => this.owner.pixiObj.destroy());
    }
  }

  checkMinionCollision() {
    let minionBB = this.scene.findObjectByName(Names.MINION).pixiObj.getBounds();
    let itemBB = this.owner.pixiObj.getBounds();

    if ( itemBB.bottom > minionBB.top && (itemBB.left < minionBB.right && itemBB.right > minionBB.left)) {
      this.sendMessage(Messages.POOP_HIT);
      this.poopIsDeleted = true;
      this.scene.invokeWithDelay(100, () => this.owner.pixiObj.destroy());
    }
  }
}
