import * as ECSA from '../libs/pixi-component';
import { Messages, Assets } from './constants';
import PIXISound from 'pixi-sound';

export class SoundComponent extends ECSA.Component {
  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_OVER, Messages.BANANA_CATCHED, Messages.BANANA_CLUMP_CATCHED,
      Messages.BANANA_PEEL_CATCHED, Messages.GAME_START, Messages.BANANA_MISSED, Messages.POOP_HIT, Messages.MONKEY);
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START) {
      PIXISound.play(Assets.SND_INTRO);
    }
    if (msg.action === Messages.GAME_OVER) {
      PIXISound.play(Assets.SND_GAMEOVER);
    }
    if (msg.action === Messages.BANANA_CATCHED || msg.action === Messages.BANANA_CLUMP_CATCHED) {
      PIXISound.play(Assets.SND_BANANA);
    }
    if (msg.action === Messages.BANANA_MISSED) {
      PIXISound.play(Assets.SND_MISSED);
    }
    if (msg.action === Messages.BANANA_PEEL_CATCHED) {
      PIXISound.play(Assets.SND_PEEL);
    }
    if (msg.action === Messages.POOP_HIT) {
      PIXISound.play(Assets.SND_SPLASH);
    }
    if (msg.action === Messages.MONKEY) {
      PIXISound.play(Assets.SND_MONKEY);
    }
  }
}
