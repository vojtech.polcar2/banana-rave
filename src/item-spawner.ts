import * as ECSA from '../libs/pixi-component';
import {BananaRaveBaseComponent} from './banana-rave-base-component';
import {checkTime} from './utils/functions';
import {Messages, States} from './constants';

export class ItemSpawner extends BananaRaveBaseComponent {
  lastSpawnTime = 0;
  spawnFrequency = 0;
  gameStarted: boolean;
  gameIsRunning: boolean;
  lastState: number;

  onInit() {
    super.onInit();
    this.subscribe(Messages.GAME_START, Messages.GAME_RUNNING, Messages.GAME_PAUSED, Messages.GAME_OVER);
    this.spawnFrequency = this.model.itemSpawnFrequency;
    this.gameStarted = false;
    this.gameIsRunning = false;
    this.lastState = States.BANANAS;
  }

  onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.GAME_START) {
      this.gameStarted = true;
      this.gameIsRunning = true;
    } else if (msg.action === Messages.GAME_PAUSED) {
      this.gameIsRunning = false;
    } else if(msg.action === Messages.GAME_RUNNING) {
      this.gameIsRunning = true;
    } else if (Messages.GAME_OVER) {
      this.gameIsRunning = false;
      this.gameStarted = false;
    }
  }

  onUpdate(delta: number, absolute: number) {
    if (this.model.score > 10 && this.model.score <= 50 && this.lastState !== States.BANANA_CLUMPS) {
      this.lastState = States.BANANA_CLUMPS;
    } else if (this.model.score > 50 && this.lastState !== States.BANANA_PEELS) {
      this.lastState = States.BANANA_PEELS;
    }

    if (this.gameIsRunning && this.gameStarted && this.model.maxItemsOnScene !== this.model.itemsOnScene) {
      if (checkTime(this.lastSpawnTime, absolute, this.spawnFrequency)) {
        this.model.itemsOnScene++;
        this.lastSpawnTime = absolute;
        this.spawnFrequency *= 1.02;
        this.factory.createItem(this.owner, new ECSA.Builder(this.owner.scene), this.model, this.lastState);
      }
    }
  }

}
