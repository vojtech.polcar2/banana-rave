import * as ECSA from '../libs/pixi-component';
import {GameModel} from './game-model';
import {
  Assets,
  Attributes,
  BLOCK_SIZE,
  Names,
  SCREEN_HEIGHT,
  SCREEN_WIDTH,
  States
} from './constants';
import MenuControllerComponent from './menu-controller-component';
import {GameManager} from './game-manager';
import {MinionControllerComponent} from './minion-controller-component';
import Dynamics from './utils/dynamics';
import {ItemSpawner} from './item-spawner';
import {ItemMovement} from './item-movement';
import {SoundComponent} from './sound-component';
import {MonkeySpawner} from './monkey-spawner';
import {MonkeyComponent} from './monkey-component';
import {PoopMovement} from './poop-movement';
export default class GameFactory {
  initializeGame(rootObject: ECSA.Container, model: GameModel) {
    let scene = rootObject.scene;
    let builder = new ECSA.Builder(scene);

    builder
      .withComponent(new SoundComponent())
      .withComponent(new GameManager())
      .withComponent(new ItemSpawner())
      .withComponent(new MonkeySpawner())
      .withComponent(new ECSA.KeyInputComponent())
      .buildInto(rootObject);

    this.buildBackground(rootObject, builder);

    this.buildPalm(rootObject, builder);

    this.buildLabels(rootObject, builder, model);

    this.buildMenu(rootObject, builder);

    let player = builder.asContainer(Names.PLAYER)
      .relativePos(0.3, 0.748)
      .withComponent(new MinionControllerComponent())
      .asSprite(PIXI.Texture.from(Assets.MINION), Names.MINION)
      .build();

    rootObject.addChild(player);
  }

  resetGame(scene: ECSA.Scene) {
    scene.clearScene();
    let model = new GameModel();
    model.loadModel(scene.app.loader.resources[Assets.DATA].data);
    scene.assignGlobalAttribute(Attributes.FACTORY, this);
    scene.assignGlobalAttribute(Attributes.MODEL, model);

    scene.invokeWithDelay(0, () => this.initializeGame(scene.stage, model));
  }

  createItem(rootObject: ECSA.Container, builder: ECSA.Builder, model: GameModel, state: States) {
    let posY = Math.random() * (model.itemSpawnMaxY - model.itemSpawnMinY) + model.itemSpawnMinY;
    let posX = Math.random() * (model.itemSpawnMaxX - model.itemSpawnMinX) + model.itemSpawnMinX;
    let velocity = Math.random() * (model.itemMaxVelocity - model.itemMinVelocity) + model.itemMinVelocity;
    let dynamics = new Dynamics();
    dynamics.velocity = new ECSA.Vector(0, velocity);

    let randomSelectItem = Math.random();
    if (state === States.BANANAS) {
      this.createBanana(rootObject, builder, model, posX, posY, dynamics);
    } else if (state === States.BANANA_CLUMPS) {
      if (randomSelectItem <= 0.7) {
        this.createBanana(rootObject, builder, model, posX, posY, dynamics);
      } else {
        this.createBananaClump(rootObject, builder, posX, posY, dynamics);
      }
    } else {
      if (randomSelectItem <= 0.6) {
        this.createBanana(rootObject, builder, model, posX, posY, dynamics);
      } else if (randomSelectItem <= 0.8) {
        this.createBananaClump(rootObject, builder, posX, posY, dynamics);
      } else {
        this.createBananaPeel(rootObject, builder, posX, posY, dynamics);
      }
    }
  }

  createBanana(rootObject: ECSA.Container, builder: ECSA.Builder, model: GameModel, posX: number, posY: number, dynamics: Dynamics) {
    builder
      .relativePos(posX, posY)
      .withParent(rootObject)
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new ItemMovement())
      .asSprite(PIXI.Texture.from(Assets.BANANA), Names.BANANA)
      .build();
  }

  createBananaClump(rootObject: ECSA.Container, builder: ECSA.Builder, posX: number, posY: number, dynamics: Dynamics) {
    builder
      .relativePos(posX, posY)
      .withParent(rootObject)
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new ItemMovement())
      .asSprite(PIXI.Texture.from(Assets.BANANA_CLUMP), Names.BANANA_CLUMP)
      .build();
  }

  createBananaPeel(rootObject: ECSA.Container, builder: ECSA.Builder, posX: number, posY: number, dynamics: Dynamics) {
    builder
      .relativePos(posX, posY)
      .withParent(rootObject)
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new ItemMovement())
      .asSprite(PIXI.Texture.from(Assets.BANANA_PEEL), Names.BANANA_PEEL)
      .build();
  }

  createMonkey(rootObject: ECSA.Container, builder: ECSA.Builder) {
    rootObject.addComponent(new MonkeyComponent(rootObject, builder));
  }

  createMonkeyPoop(rootObject: ECSA.Container, builder: ECSA.Builder, posX: number, posY: number, dynamics: Dynamics) {
    builder.relativePos(posX / SCREEN_WIDTH, posY / SCREEN_HEIGHT)
      .withParent(rootObject)
      .withAttribute(Attributes.DYNAMICS, dynamics)
      .withComponent(new PoopMovement())
      .asSprite(PIXI.Texture.from(Assets.MONKEY_POOP), Names.MONKEY_POOP)
      .build();
  }

  createHittedMinion(rootObject: ECSA.Container, builder: ECSA.Builder) {
    let minionPos = rootObject.scene.findObjectByName(Names.MINION).getBounds();
    let hittedMinion = builder
      .relativePos(minionPos.x / SCREEN_WIDTH, minionPos.y / SCREEN_HEIGHT)
      .asSprite(PIXI.Texture.from(Assets.MINION_HITTED), Names.MINION_HITTED)
      .withParent(rootObject)
      .build();

    rootObject.scene.invokeWithDelay(1900, () => hittedMinion.pixiObj.destroy());
  }

  buildPalm(rootObject: ECSA.Container, builder: ECSA.Builder) {
    /* LOG */
    for (let i = 0; i < SCREEN_HEIGHT/(BLOCK_SIZE/2) - 10; i++) {
      let posY = (BLOCK_SIZE/2) * i;
      let palmPart = builder
        .asGraphics(Names.PALM_PART)
        .relativePos(0.45,1 - 0.187 - (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmPart.asGraphics().beginFill(0xCD853F);
      palmPart.asGraphics().drawRect(0,0, BLOCK_SIZE, BLOCK_SIZE/2);
      palmPart.asGraphics().endFill();
    }

    /* LEAFS */
    for (let i = 0; i < SCREEN_WIDTH/BLOCK_SIZE - 2; i++) {
      let posX = BLOCK_SIZE * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.915 + (posX / SCREEN_WIDTH), 0.2)
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < SCREEN_WIDTH/BLOCK_SIZE - 4; i++) {
      let posX = BLOCK_SIZE * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.84 + (posX / SCREEN_WIDTH), 0.232)
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < SCREEN_WIDTH/BLOCK_SIZE - 3; i++) {
      let posX = BLOCK_SIZE * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.875 + (posX / SCREEN_WIDTH), 0.18)
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < SCREEN_WIDTH/BLOCK_SIZE - 4; i++) {
      let posX = BLOCK_SIZE * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.84 + (posX / SCREEN_WIDTH), 0.14)
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 6; i++) {
      let posX = (BLOCK_SIZE/2) * i;
      let posY = (BLOCK_SIZE/4) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.7 - (posX / SCREEN_WIDTH), 0.17 - (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 4; i++) {
      let posX = (BLOCK_SIZE/2.85) * i;
      let posY = (BLOCK_SIZE/3) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.6 - (posX / SCREEN_WIDTH), 0.16 - (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 7; i++) {
      let posX = (BLOCK_SIZE/2.5) * i;
      let posY = (BLOCK_SIZE/5) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.5 + (posX / SCREEN_WIDTH), 0.147 - (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 5; i++) {
      let posX = (BLOCK_SIZE/4) * i;
      let posY = (BLOCK_SIZE/7) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.25 + (posX / SCREEN_WIDTH), 0.137 - (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 4; i++) {
      let posX = (BLOCK_SIZE/3) * i;
      let posY = (BLOCK_SIZE/3) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.3 + (posX / SCREEN_WIDTH), 0.237 + (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 3; i++) {
      let posX = (BLOCK_SIZE/5) * i;
      let posY = (BLOCK_SIZE/6) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.4 - (posX / SCREEN_WIDTH), 0.25 + (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }

    for (let i = 0; i < 4; i++) {
      let posX = (BLOCK_SIZE/2) * i;
      let posY = (BLOCK_SIZE/3) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.58 - (posX / SCREEN_WIDTH), 0.24 + (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }


    for (let i = 0; i < 6; i++) {
      let posX = (BLOCK_SIZE/2) * i;
      let posY = (BLOCK_SIZE/5) * i;
      let palmLeaf = builder
        .asGraphics(Names.PALM_LEAF)
        .relativePos(1 - 0.72 - (posX / SCREEN_WIDTH), 0.24 + (posY / SCREEN_HEIGHT))
        .withParent(rootObject)
        .build();

      palmLeaf.asGraphics().beginFill(0x228B22);
      palmLeaf.asGraphics().drawRect(0,0,BLOCK_SIZE, BLOCK_SIZE/2);
      palmLeaf.asGraphics().endFill();
    }
  }

  buildBackground(rootObject: ECSA.Container, builder: ECSA.Builder) {
    let ground = builder.asGraphics(Names.GROUND)
      .relativePos(0,0)
      .build();
    ground.asGraphics().beginFill( 0x654321);
    ground.asGraphics().drawRect(0,SCREEN_HEIGHT - (BLOCK_SIZE/2) - BLOCK_SIZE, SCREEN_WIDTH, BLOCK_SIZE - (BLOCK_SIZE/2));
    ground.asGraphics().endFill();
    rootObject.pixiObj.addChild(ground);

    let grass = builder.asGraphics(Names.GRASS)
      .relativePos(0,0)
      .build();
    grass.asGraphics().beginFill(0x006600);
    grass.asGraphics().drawRect(0, SCREEN_HEIGHT - ((BLOCK_SIZE/4)*3) - BLOCK_SIZE, SCREEN_WIDTH, BLOCK_SIZE/4);
    grass.asGraphics().endFill();
    rootObject.pixiObj.addChild(grass);

    let sky = builder.asGraphics(Names.SKY)
      .relativePos(0,0)
      .build();
    sky.asGraphics().beginFill(0x479cea);
    sky.asGraphics().drawRect(0,0, SCREEN_WIDTH, SCREEN_HEIGHT - (BLOCK_SIZE/4)*3 - BLOCK_SIZE);
    sky.asGraphics().endFill();
    rootObject.pixiObj.addChild(sky);
  }

  buildLabels(rootObject: ECSA.Container, builder: ECSA.Builder, model: GameModel) {
    /* SCORE LABEL */
    builder
      .relativePos(0.02,0.95).anchor(0,1)
      .withComponent(new ECSA.GenericComponent('ScoreComponent').doOnUpdate((cmp )=> {
        cmp.owner.asText().text = 'SCORE: '+ Math.floor(model.score);
      }))
      .asText(Names.SCORE,'SCORE: ', new PIXI.TextStyle({
        fill: 'white',
        fontSize: 16
      }))
      .withParent(rootObject)
      .build();

    /* BANANAS LABEL */
    builder
      .relativePos(0.83,0.95).anchor(0,1)
      .withComponent(new ECSA.GenericComponent('MissedBananasComponent').doOnUpdate((cmp )=> {
        cmp.owner.asText().text = 'Missed: '+ model.missedBananas + '/' + model.maxMissedBananas;
      }))
      .asText(Names.MISSED_BANANAS,'Missed: ', new PIXI.TextStyle({
        fill: 'white',
        fontSize: 16
      }))
      .withParent(rootObject)
      .build();

    /* GAME OVER LABEL */
    builder.relativePos(0.3, 0.4).anchor(0,0)
      .asText(Names.GAMEOVER, 'GAME OVER', new PIXI.TextStyle({
        fill: 'white',
        dropShadow: true,
        fontSize: 36
      }))
      .withParent(rootObject)
      .build().visible = false;
    rootObject.scene.findObjectByName(Names.GAMEOVER).pixiObj.zIndex = 100;

    builder.relativePos(0.25, 0.5).anchor(0,0)
      .asText(Names.CONTINUE, 'Press SPACE to continue', new PIXI.TextStyle({
        fill: 'white',
        fontSize: 26
      }))
      .withParent(rootObject)
      .build().visible = false;
    rootObject.scene.findObjectByName(Names.CONTINUE).pixiObj.zIndex = 100;
  }

  buildMenu(rootObject: ECSA.Container, builder: ECSA.Builder) {
    let menuContainer = builder.asContainer(Names.MENU)
      .relativePos(0.3, 0.14)
      .anchor(0.3,0.3)
      .withComponent(new MenuControllerComponent())
      .withParent(rootObject)
      .build();
    rootObject.scene.findObjectByName(Names.MENU).pixiObj.zIndex = 100;

    /* GAME TITLE */
    builder
      .relativePos(0, 0)
      .anchor(0, 0)
      .asText('bananaRaveTitle', 'BANANA RAVE', new PIXI.TextStyle({
          fill: 'white',
          dropShadow: true,
          fontSize: 30
      }))
      .withParent(menuContainer)
      .build();

    /* START/PAUSE BUTTON */
    builder
      .relativePos(0,0.07)
      .anchor(0, 0)
      .asText('menuButton', 'Press SPACE to start/pause the game', new PIXI.TextStyle ( {
        fill: 'white',
        fontSize: 15,
      }))
      .withParent(menuContainer)
      .build();

    /* RESTART BUTTON */
    builder
      .relativePos(0,0.1)
      .anchor(0, 0)
      .asText('restartButton', 'Press R to restart', new PIXI.TextStyle ( {
        fill: 'white',
        fontSize: 15,
      }))
      .withParent(menuContainer)
      .build();
  }
}
